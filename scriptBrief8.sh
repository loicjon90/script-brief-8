az login

###### Création d'un Groupe de ressource ########

echo "Le nom de votre groupe de ressource"

read nomGroupe

echo "Localisation de votre groupe de ressource"

read localisation

echo "Creation du groupe de ressource en cours..."
az group create \
    --name $nomGroupe\
    --location $localisation

echo "Groupe de ressource "$nomGroupe" crée"

####### Création Plan Aure service ############

echo "Entrer nom du plan Azure service"
read nomPlan 

echo "Creation du Plan service en cours..."

az appservice plan create --name $nomPlan --resource-group $nomGroupe --location $localisation --is-linux --per-site-scaling --sku P1V2

echo "plan azure service "$nomPlan" crée"

###### Création Webapp #################

echo "Entrer nom de la webapp"
read nomWebapp

echo "Creation de la webapp en cours..."
az webapp create --name $nomWebapp --plan $nomPlan --resource-group $nomGroupe --runtime "PHP:8.0"
echo "Webapp "$nomWebapp" crée"

############ Création Mariadb ##############

echo "Entrer nom du service db pour Mariadb"
read nomMariadb
echo "Entrer nom admin mariadb"
read adminMariadb
echo "Entrer pass pour admin mariadb"
read adminMariadbPass

echo "Creation du service db pour Mariadb en cours..."
az mariadb server create --name $nomMariadb --resource-group $nomGroupe --location westeurope --admin-user $adminMariadb --admin-password $adminMariadbPass --sku-name GP_Gen5_2 --version 10.3 --public all --ssl-enforcement Disabled
echo "Service db pour Mariadb "$nomMariadb" crée"


############ Creéation compte de stockage ############

echo "entre le nom pour le compte de stockage"
read mystorageaccount 
echo "Creation du compte de stockage en cours"
az storage account create -n $mystorageaccount -g  $nomGroupe -l francecentral --sku Standard_LRS



########## Suppression des ressources ##############""

echo "Souhaitez vous supprimer les ressources crées ? (Y=Yes , N=No)"
read suppression
if [[($suppression == "y")]]; then
echo "Début de la suppression des ressources"
az webapp delete --name $nomWebapp --resource-group $nomGroupe
echo "suppression de la webapp en cours ..."
echo "webapp supprimée ..."
az mariadb server delete --resource-group $nomGroupe --name $nomMariadb
echo "suppression de mariadb en cours ..."
echo "mariadb db supprimée ..."
az appservice plan delete --name $nomPlan --resource-group $nomGroupe
echo "suppression du app service plan en cours ..."
echo "app service plan supprimé ..."
az group delete --name $nomGroupe
echo "suppression du groupe de ressource en cours ..."
echo "groupe de ressource supprimé ..."
echo "Toutes les ressources précedement crées ont été supprimées!"
else
echo "aurevoir "
fi
```

